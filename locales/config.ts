export default {
  locales: [
    {
      code: 'vi',
      iso: 'vi-VN',
      name: 'Vietnamese',
      file: 'vi.json'
    },
    {
      code: 'en',
      iso: 'en-EN',
      name: 'English',
      file: 'en.json'
    },
  ],
  defaultLocale: 'en',
  detectBrowserLanguage: {
    useCookie: true,
    cookieKey: 'my_custom_cookie_name',
    alwaysRedirect: true,
    // redirectOn: 'root',
    onlyOnRoot: true
    // useCookie: false,
    // cookieCrossOrigin: true,
    // cookieKey: 'my_custom_cookie_name'
  },
  baseUrl: '',
  seo: true,
  lazy: true,
  parsePages: false,
  vueI18n: {
    fallbackLocale: ['en', 'vi',]
  },
  langDir: 'locales/',
  encodePaths: false,
  // strategy: 'no_prefix',
}

import { Expose } from "class-transformer";
import { Home } from "../Home/Home";
import { UserInfo } from "../User/UserInfo";

export class Booking {
    @Expose({ name: "id" }) id?: string;
    @Expose({name: 'user_info'}) user_info?: UserInfo;
    @Expose({ name: "home" }) home?: Home;
    @Expose({name: 'num_of_adults'}) numOfAdults?: Number;
    @Expose({name: 'num_of_children'}) numOfChildren?: Number;
    @Expose({name: 'num_of_babies'}) numOfBabies?: Number;
    @Expose({name: 'num_of_pets'}) numOfPets?: Number;
    @Expose({ name: "check_in_date" }) checkIn?: Date;
    @Expose({ name: "check_out_date" }) checkOut?: Date;
    @Expose({ name: "created_at" }) createdAt?: Date;
    @Expose({ name: "updated_at" }) updatedAt?: Date;
    @Expose({ name: "price_per_day" }) pricePerDay?: Number;
    @Expose({ name: "price_for_stay" }) priceForStay?: Number;
    @Expose({ name: "status" }) status?: string;
    @Expose({ name: "rating" }) rating?: string;
    @Expose({name: "refund"}) refund?: Boolean;
}
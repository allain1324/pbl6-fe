import { Expose } from "class-transformer";

class Review{
    @Expose({ name: "idUser" }) idUser?: string;
    @Expose({ name: "review" }) review?: string;
    @Expose({ name: "dateCreate" }) dateCreate?: Date;
    @Expose({ name: "idHome" }) idHome?: string;
}

export class Rating {
  @Expose({name: "cleanliness"}) cleanliness?: Number;
  @Expose({name: "accuracy"}) accuracy?: Number;
  @Expose({name: "communication"}) communication?: Number;
  @Expose({name: "checkIn"}) checkIn?: Number;
  @Expose({name: "location"}) location?: Number;
  @Expose({ name: "value" }) value?: Number;
  @Expose({ name: "reviewResponses"}) reviewResponses?: Array<Review>;  
}
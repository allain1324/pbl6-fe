import { Expose } from "class-transformer";
import { Category } from "./Category";

class User {
  @Expose({name: "id"}) id?: string;
  @Expose({name: "idUser"}) idUser?: string;
  @Expose({name: "name"}) name?: string;
  @Expose({name: "phone"}) phone?: string;
  @Expose({name: "address"}) address?: string;
  @Expose({name: "avatar"}) avatar?: string;
}

class Amenity {
  @Expose({ name: "id" }) id?: Number;
  @Expose({ name: 'title' }) title?: string;
  @Expose({ name: 'icon' }) icon?: string;
}

export class Home {
  @Expose({name: "id"}) id?: string;
  @Expose({name: "name"}) name?: string;
  @Expose({name: "description"}) description?: string;
  @Expose({name: "address"}) address?: string;
  @Expose({name: "numOfGuests"}) numOfGuests?: Number;
  @Expose({name: "numOfBeds"}) numOfBeds?: Number;
  @Expose({name: "numOfBedrooms"}) numOfBedrooms?: Number;
  @Expose({name: "numOfBathrooms"}) numOfBathrooms?: Number;
  @Expose({name: "price"}) price?: Number;
  @Expose({name: "priceService"}) priceService?: Number;
  @Expose({name: "category"}) category?: Category;
  @Expose({name: "bathroom"}) bathroom?: Array<Amenity>;
  @Expose({name: "bedroomAndLaundry"}) bedroomAndLaundry?: Array<Amenity>;
  @Expose({name: "entertainment"}) entertainment?: Array<Amenity>;
  @Expose({name: "family"}) family?: Array<Amenity>;
  @Expose({name: "heatingAndCooling"}) heatingAndCooling?: Array<Amenity>;
  @Expose({name: "homeSafety"}) homeSafety?: Array<Amenity>;
  @Expose({name: "internetAndOffice"}) internetAndOffice?: Array<Amenity>;
  @Expose({name: "internetAndOffice"}) kitchenAndDining?: Array<Amenity>;
  @Expose({name: "locationFeature"}) locationFeature?: Array<Amenity>;
  @Expose({name: "outdoor"}) outdoor?: Array<Amenity>;
  @Expose({name: "parkingAndFacilities"}) parkingAndFacilities?: Array<Amenity>;
  @Expose({name: "services"}) services?: Array<Amenity>;
  @Expose({name: "files"}) listImages?: Array<string>;
  @Expose({ name: "user" }) user?: User;
  @Expose({ name: "startAt" }) startAt?: Date;
  @Expose({ name: "endAt" }) endAt?: Date;
  @Expose({ name: "dateCreate" }) dateCreate?: Date;
  @Expose({name: "status" }) status?: String;
}
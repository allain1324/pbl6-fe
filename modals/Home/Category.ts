import { Expose } from "class-transformer";

export class Category {
  @Expose({name: "id"}) id?: string;
  @Expose({name: "title"}) title?: string;
  @Expose({name: "icon"}) icon?: string;
  @Expose({name: "description"}) description?: string;
}
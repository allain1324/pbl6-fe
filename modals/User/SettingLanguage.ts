import { Expose } from "class-transformer";

export class SettingLanguage {
  @Expose({name: "id"}) id?: string;
  @Expose({name: "name"}) nameLanguage?: string;
  @Expose({name: "code"}) languageCode?: string;
}
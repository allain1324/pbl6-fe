import { Expose } from "class-transformer";
import { SettingLanguage } from './SettingLanguage';
import { UserInfo } from "./UserInfo";

export class User {
  @Expose({name: "id"}) id?: string;
  @Expose({name: "email"}) email?: string;
  @Expose({name: "role"}) role?: string;
  @Expose({name: "setting_language"}) setting_language?: SettingLanguage;
  @Expose({name: "user_info"}) user_info?: UserInfo;
}
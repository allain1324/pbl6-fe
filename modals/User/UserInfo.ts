import { Expose } from "class-transformer";

export class UserInfo {
  @Expose({name: "idUser"}) id?: string;
  @Expose({name: "name"}) name?: string;
  @Expose({name: "phone"}) phone?: string;
  @Expose({ name: "address" }) address?: string;
  @Expose({ name: "avatar" }) avatar?: string;
}


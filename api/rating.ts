export default axios => ({
    ratingRoom(data) {
        return axios.post('/rating', data);
    },

    getRatingHome(id) {
        return axios.get(`/rating/${id}`);
    }
})
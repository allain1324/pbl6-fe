export default axios => ({
    getUserInfo(id) {
        return axios.get(`/user/${id}`);
    }
})
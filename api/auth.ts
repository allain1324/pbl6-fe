export default axios => ({
    signupAuth(data) {
        return axios.post('/auth/signup', data);
    },

    changePasswordAuth(oldPassword, newPassword, confirmPassword) {
        return axios.post('/auth/password-change', {
            old_password: oldPassword,
            new_password: newPassword,
            confirm_new_password: confirmPassword
        }).catch(err => {
            console.log(err);
        });
    },

    forgetPasswordAuth(email) {
        return axios.post('/auth/password-forget', {email})
    },

    resetPasswordAuth(password, confirmation_password, confirm_token) {
        return axios.post('/auth/password-reset', {password, confirmation_password, confirm_token})
    },
    getUserInfoAuth(){
        return axios.get(`/auth/userinfo`);
    }

})
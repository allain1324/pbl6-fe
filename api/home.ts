export default axios => ({
  getCategoriesHome(){
    return axios.get('/home/getCategories').catch(err => {
      console.log(err);
    })
  },

  getAllOptionHome(){
    return Promise.all([
      axios.get('/home/getBathroom'),
      axios.get('/home/getBedroomAndLaundry'),
      axios.get('/home/getEntertainment'),
      axios.get('/home/getFamily'),
      axios.get('/home/getheatingAndCooling'),
      axios.get('/home/getHomeSafety'),
      axios.get('/home/getInternetAndOffice'),
      axios.get('/home/getKitchenAndDining'),
      axios.get('/home/getLocationFeature'),
      axios.get('/home/getOutdoor'),
      axios.get('/home/getParkingAndFacilities'),
      axios.get('/home/getServices'),
    ]).catch(err => {
      console.log(err);
    })
  },

  getAllHome(){
    return axios.get('/home/').catch(e => {
      
    });
  },
  getAllHomeByUser(idUser) {
    return axios.get(`home/getHomeByIdUser?idUser=${idUser}`);
  },
  getAllCategories(){
    return axios.get('/home/getCategories');
  },
  getHomeByCategoryHOME(nameCategory, paging, page){
    return axios.get(`/home?category=${nameCategory}&paging=${paging}&page=${page}`);
  },
  getHomeDetail(id){
    return axios.get(`/home/${id}`);
  },

  uploadImage(){
    
  }
})
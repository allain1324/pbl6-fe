export default axios => ({
    bookingRoom(data) {
        return axios.post("/booking", data);
    },

    getBookingByClient() {
        return axios.get("/booking");
    },

    getBookingDetailsByClient(id) {
        return axios.get(`/booking/${id}`);
    },

    getAllBookingsByOwner() {
        return axios.get("/owner/booking")
    },

    cancelBookingByClient(id, reason) {
        return axios.patch(`/booking/${id}/cancel`, {reason});
    }
})
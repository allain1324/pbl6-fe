import Vue from 'vue'

interface Device {
  userAgent: string
  isDesktop: boolean
  isIos: boolean
  isAndroid: boolean
  isMobile: boolean
  isMobileOrTablet: boolean
  isDesktopOrTablet: boolean
  isTablet: boolean
  isWindows: boolean
  isMacOS: boolean
  isSafari: boolean
  isFirefox: boolean
  isEdge: boolean
  isChrome: boolean
  isSamsung: boolean
  isCrawler: boolean
}

declare module "vue/types/vue" {
  interface Vue {
    $isIE: boolean;
    $isFirefox: boolean;
    $isiOS: boolean;
    $device: Device;
    $api: any;
  }
}

declare module 'vuex/types/index' {
  // this.$myInjectedFunction inside Vuex stores
  interface Store<S> {
    $api: any;
  }
}

declare module '@nuxt/types' {
  interface Context {
    $api: any;
  }
}
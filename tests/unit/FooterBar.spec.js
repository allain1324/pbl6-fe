import { mount, shallowMount } from "@vue/test-utils";
import FooterBar from "@/components/FooterBar.vue";

const factory = () => {
  return shallowMount(FooterBar, {
    stubs: {
      NuxtLink: true,
    },
  });
};

describe("FooterBar", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(FooterBar, {
      stubs: {
        NuxtLink: true,
      },
    });
    expect(wrapper.vm).toBeTruthy();
  });

  test("renders properly", () => {
    const wrapper = factory();
    expect(wrapper.html()).toMatchSnapshot();
  });
});

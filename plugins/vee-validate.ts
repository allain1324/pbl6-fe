import Vue from 'vue'
import VeeValidate, {Validator, Rules} from 'vee-validate'

Vue.use(VeeValidate, {
  inject: true
});
export default (context, inject) => {
  const handleErrorFromApi = (err, callback) => {
    let _error = ''
    if(!err.response){
      _error = ''
    }
    else {
      _error = err.response?.data?.error?.code ?? '';
    }
    _error.replace('.', '')
    if(typeof callback === 'function'){
      callback(_error); 
    }
  }

  inject("handleErrorFromApi", handleErrorFromApi);
}
import axios from "axios";

export default function ({ $axios, redirect, error }) {
  $axios.onRequest((config) => {
    config.headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
    };
    console.log("Making request to " + config.url);
    // if(config.url.includes( "/user/")){
    //   config.headers.Authorization = undefined;
    // }
  });

  $axios.onError((err) => {
    console.log("Error: " + err);
    const code = parseInt(err.response && err.response.status);
    if (code === 400) {
      console.log(err);
      
      error({ statusCode: code, message: err.response });
    }
  });
}

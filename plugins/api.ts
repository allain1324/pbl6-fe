import Auth from "~/api/auth";
import Home from "~/api/home";
import User from "~/api/user";
import Booking from "~/api/booking";
import Rating from "~/api/rating";

export default (context, inject) => {
  // Initialize API factories
  const factories = {
    auth: Auth(context.$axios),
    home: Home(context.$axios),
    user: User(context.$axios),
    booking: Booking(context.$axios),
    rating: Rating(context.$axios),
  };

  // Inject $api
  inject("api", factories);
};

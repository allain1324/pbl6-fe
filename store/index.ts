import { plainToClass } from "class-transformer";
import { GetterTree, ActionContext, ActionTree, MutationTree } from "vuex";

export const types = {};

export interface RootState {}

export interface State {}

export const state = (): State => ({});

export const rootState = (): RootState => ({});

export const getters: GetterTree<State, RootState> = {};

export interface Actions<S, R> extends ActionTree<S, R> {
  nuxtServerInit(context: ActionContext<S, R>, any: any): Promise<any>;
}

export const actions: Actions<State, RootState> = {
  nuxtServerInit({ commit, dispatch, state }, { req, app, query }) {
        
    return Promise.all([
      dispatch("category/fetchListCategory"),
    ]);

  },
};
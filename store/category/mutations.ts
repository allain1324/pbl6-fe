import { MutationTree } from 'vuex'
import { Category1 } from './state'
import types from './types'

const mutations: MutationTree<Category1> = {
  [types.FETCH_LIST_CATEGORY](state, categories) {
    state.categories = [...categories];
  }
}

export default mutations
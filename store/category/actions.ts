import { ActionTree } from 'vuex'
import { RootState } from 'store'
import { Category1 } from './state'
import { Category } from '~/modals/Home/Category'
import types from './types'
import { plainToClass } from 'class-transformer'

export interface Actions<S, R> extends ActionTree<S, R> {}

const actions: Actions<Category1, RootState> = {
  async fetchListCategory({commit, state}) { 
    try{
      if(state.categories.length > 0) return;
      let results: any = {};
      results = await this.$api.home.getCategoriesHome();
      const list = plainToClass(Category, results["data"]["data"])
      commit(types.FETCH_LIST_CATEGORY, list)
    } catch(error){
      console.log("fetchListCategory", error)
    }
  },

  async fetchListOptionHome({commit, state}){
    try {
      if(state.listOptionsHome.length > 0) return;
      let results: Object[] = [];
      results = await this.$api.home.getAllOptionHome();
      console.log("results", results)
    }catch(e) {

    }
  }
}

export default actions
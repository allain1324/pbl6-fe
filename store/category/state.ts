import { Category } from "~/modals/Home/Category";

export interface Category1 {
  categories: Category[];
  listOptionsHome: Object[]
}

const state = (): Category1 => ({
  categories: [], // danh sách loại phòng
  listOptionsHome: [] // danh sách tiện ích
})

export default state;
import i18nConfig from "./locales/config";

export default {
  server: {
    port: 3003,
    host: "0.0.0.0",
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Euphoria Viet Nam",
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0",
      },
      { httpEquiv: "X-UA-Compatible", content: "IE=Edge" },
      { hid: "description", name: "description", content: "Euphoria Viet Nam" },
      { hid: "og:title", property: "og:title", content: "Euphoria" },
      {
        hid: "og:description",
        property: "og:description",
        content:
          "Find vacation rentals, cabins, beach houses, unique homes and experiences around the world - all made possible by hosts on Euphoria.",
      },
      {
        hid: "og:image",
        property: "og:image",
        content: "/images/euphoria-banner.png",
      },
      { hid: "og:site_name", property: "og:site_name", content: "Euphoria" },
      {
        hid: "og:url",
        property: "og:url",
        content: "https://euphoria-vn.com/",
      },
      //meta twitter
      {
        hid: "twitter:card",
        name: "twitter:card",
        content: "summary_large_image",
      },
      { hid: "twitter:site", name: "twitter:site", content: "@Euphoria" },
      { hid: "twitter:title", name: "twitter:title", content: "Euphoria" },
      {
        hid: "twitter:description",
        name: "twitter:description",
        content:
          "Find vacation rentals, cabins, beach houses, unique homes and experiences around the world - all made possible by hosts on Euphoria.",
      },
      {
        hid: "twitter:image",
        name: "twitter:image",
        content: "/images/euphoria-banner.png",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/scss/argon.scss",
    "~/assets/scss/app.scss",
    "quill/dist/quill.snow.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/bootstrap-vue.js",
    { src: "~/plugins/vee-validate", ssr: false },
    { src: "~/plugins/vue-lazyload", ssr: false },
    { src: "~plugins/vue-calendar", ssr: false },
    { src: "~/plugins/vue-click-outside", ssr: false },
    { src: "~/plugins/axios", ssr: true },
    { src: "~/plugins/v-scroll-lock", ssr: true },
    { src: "~/plugins/vue-select", ssr: false },
    { src: "~/plugins/api" },
    "~plugins/custom-flag",
    { src: "~plugins/vue-quill-editor.ts", ssr: false },
    { src: "~/plugins/infiniteloading", ssr: false },
    { src: "~/plugins/handleErrorApi" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    "@nuxtjs/google-analytics",
    "@nuxtjs/auth-next",
    "@nuxtjs/device",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "bootstrap-vue/nuxt",
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    // https://go.nuxtjs.dev/content
    "@nuxt/content",

    "@nuxtjs/style-resources",

    "@nuxtjs/sentry",
    // '@nuxtjs/google-analytics',
    // '@nuxtjs/auth-next',

    ["@nuxtjs/i18n", i18nConfig],
  ],

  sentry: {
    dsn: "https://aae39b0d89e84b75a7f60532c84256b7@o4504282855833600.ingest.sentry.io/4504283050016768", // Enter your project's DSN here
    // Additional Module Options go here
    // https://sentry.nuxtjs.org/sentry/options
    tracesSampleRate: 1.0,
    config: {
      // Add native Sentry config here
      // https://docs.sentry.io/platforms/javascript/guides/vue/configuration/options/
    },
  },

  device: {
    refreshOnResize: true,
  },
  styleResources: {
    scss: ["~/assets/scss/variables.scss"],
  },

  googleAnalytics: {
    id: "G-55S9J9DXWC",
  },
  publicRuntimeConfig: {
    googleAnalytics: {
      id: "G-55S9J9DXWC",
    },
    "google-gtag": {
      id: "G-55S9J9DXWC",
    },
  },
  "google-gtag": {
    id: "G-55S9J9DXWC",
  },
  bootstrapVue: {
    icons: true,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.BASE_URL_API,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "en",
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      compact: true,
    },
  },

  router: {},

  auth: {
    // redirect: {
    //   callback: '/api/oauth2/callback/google',
    //   login: '/login',
    //   home: '/forums',
    //   logout: undefined
    // },
    strategies: {
      local: {
        scheme: "refresh",
        token: {
          property: "data.access_token",
          maxAge: "data.expires_in",
        },
        refreshToken: {
          property: "data.refresh_token",
          data: "refresh_token",
          maxAge: 60 * 60 * 24 * 30,
        },
        user: {
          property: "data",
          // autoFetch: true
        },
        endpoints: {
          login: { url: "/auth/signin", method: "post" },
          user: { url: "/auth/userinfo", method: "get" },
          refresh: { url: "/auth/refreshtoken", method: "post" },
          logout: { url: "/auth/logout", method: "post" },
        },
      },
    },
  },
};

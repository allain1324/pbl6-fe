export default function ({route, $auth, redirect, app}){
  if(!$auth.loggedIn){
    return redirect(app.localePath('/'));
  }
}